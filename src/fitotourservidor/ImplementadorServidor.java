/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitotourservidor;

import constantes.HospedagemBean;
import constantes.InteresseBean;
import constantes.PassagemBean;
import interfaces.InterfaceServidor;
import interfaces.InterfaceCliente;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

/**
 *
 * @author Tito
 */
public class ImplementadorServidor extends UnicastRemoteObject implements InterfaceServidor {

    public ImplementadorServidor() throws RemoteException {

    }

    @Override
    public void chamar(String nomeCliente, InterfaceCliente refCliente) throws RemoteException {
        refCliente.echo("Oi, estou dando echo em nome de: " + nomeCliente);
    }

    @Override
    /**
     * Método que chama a batata
     */
    public void batata() throws RemoteException {
        System.out.println("Alguem chamou a batata?");
    }

    /**
     * Retorna ao cliente a lista de passagens disponíveis
     *
     * @return
     * @throws java.rmi.RemoteException
     */
    @Override
    public List<PassagemBean> listarPassagensDisponiveis() throws RemoteException {
        System.out.println("Tamanho passagens via server:" + DadosSingletonServidor.getInstance().getPassagensSize());
        return DadosSingletonServidor.getInstance().getPassagensDisponiveis();
    }

    @Override
    public void oiCliente(InterfaceCliente usuario) throws RemoteException {
        DadosSingletonServidor.getInstance().getClientes().add(usuario);
        System.out.println("Usuário adicionado no pool de interface de usuarios!");
    }

    @Override
    public void removerCliente(InterfaceCliente cliente) throws RemoteException {
        DadosSingletonServidor.getInstance().removerCliente(cliente);
    }

    @Override
    public void adicionarInteresse(InteresseBean inter) throws RemoteException {
        DadosSingletonServidor.getInstance().addInteresse(inter);
    }

    /**
     * Deve editar a passagem que foi editada lá no cliente Ela usa o indice
     * para editar a passagem, mas não é exatamente o indice do singleton, mas o
     * indice das passagens ainda não compradas.
     *
     * @param passagem
     * @param index
     * @throws RemoteException
     */
    @Override
    public void editarPassagem(PassagemBean passagem, int index) throws RemoteException {
        /*
        System.out.println("Dados da passagem atualizada recebida no server:");
        System.out.println("Informacao da passagem no vetor de passagens:");
        System.out.println("Data: " + passagem.getDataString());
        System.out.println("Destino: " + passagem.getDestino());
        System.out.println("Origem: " + passagem.getOrigem());
        System.out.println("Nome passageiro: " + passagem.getPassageiro().getNome());
        System.out.println("Idade: " + passagem.getPassageiro().getIdade());
         */
        for (PassagemBean pas : DadosSingletonServidor.getInstance().getPassagens()) {
            //So o objeto pas for:
            //De data, destino, preço e origem iguais
            //E for nula
            //Substitui aquele item pelo objeto novo.
            if (pas.getPreco() == passagem.getPreco() && pas.getData().equals(passagem.getData()) && pas.getDestino().equals(passagem.getDestino()) && pas.getOrigem().equals(passagem.getOrigem()) && pas.getPassageiro() == null) {
                pas.setCartao(passagem.getCartao());
                pas.setNumeroDeParcelas(passagem.getNumeroDeParcelas());
                pas.setPassageiro(passagem.getPassageiro());
                break;
            }
        }
        //Atualiza a tabela da interface gráfica do servidor:
        DadosSingletonServidor.getInstance().atualizarTabelaPassagem();
        //Atualiza as tabelas da interace gráfica de todos os usuários:
        DadosSingletonServidor.getInstance().atualizarTabelaPassagemClientes();
        System.out.println("Passagem editada agora pertence a " + passagem.getPassageiroString());
    }

    @Override
    public void editarHospedagem(HospedagemBean hospedagem) throws RemoteException {
        //Editando a hospedagem na lista central de hospedagens singleton.
        for (HospedagemBean hosp : DadosSingletonServidor.getInstance().getDiarias()) {
            if (hosp.getOcupante() == null
                    && hosp.getCidade().equals(hospedagem.getCidade())
                    && hosp.getCapacidade() == hospedagem.getCapacidade()
                    && hosp.getDia().equals(hospedagem.getDia())) {
                hosp.setOcupante(hospedagem.getOcupante());
                hosp.setCartao(hospedagem.getCartao());
                hosp.setNumeroDeParcelas(hospedagem.getNumeroDeParcelas());
                break;
            }
        }
        //Atualizar tabela
        DadosSingletonServidor.getInstance().modeloTabelaHospedagem.fireTableDataChanged();
        //Espalhar atualização
        DadosSingletonServidor.getInstance().broadcastNovasHospedagens();
    }

    @Override
    public List<HospedagemBean> getHospedagens() throws RemoteException {
        List<HospedagemBean> lista;
        lista = DadosSingletonServidor.getInstance().getDiarias();
        return lista;
    }

    @Override
    public void chamarBroadcastHospedagens() throws RemoteException {
        DadosSingletonServidor.getInstance().broadcastNovasHospedagens();
    }

}
