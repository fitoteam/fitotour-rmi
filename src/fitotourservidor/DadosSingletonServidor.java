/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitotourservidor;

import constantes.Constantes;
import constantes.InteresseBean;
import constantes.HospedagemBean;
import constantes.PassagemBean;
import constantes.TabelaHospedagem;
import interfaces.InterfaceCliente;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Tito
 */
public class DadosSingletonServidor {

    private List<InterfaceCliente> clientes = new ArrayList();
    private List<PassagemBean> passagens = new ArrayList();
    private List<HospedagemBean> diarias = new ArrayList();
    private List<InteresseBean> interesses = new ArrayList();
    private DefaultTableModel tabelaDePassagens;
    private DefaultTableModel tabelaInteresses;
    public TabelaHospedagem modeloTabelaHospedagem;

    //Métodos necessários para o singleton
    private DadosSingletonServidor() {
    }

    public List<PassagemBean> getPassagens() {
        return passagens;
    }

    public void setPassagens(List<PassagemBean> passagens) {
        this.passagens = passagens;
    }

    public void iniciarServidor() {
        try {
            Registry referenciaServicoNomes = LocateRegistry.createRegistry(1099);
            referenciaServicoNomes.bind("SERVIDOR", new ImplementadorServidor());
        } catch (RemoteException | AlreadyBoundException ex) {
            Logger.getLogger(DadosSingletonServidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static DadosSingletonServidor getInstance() {
        return SingletonListaDeClientesHolder.INSTANCE;
    }

    public void atualizarTabelaPassagemClientes() {
        try {
            for (InterfaceCliente intf : clientes) {
                intf.atualizarTabelaDePassagens();
            }
        } catch (RemoteException ex) {
            Logger.getLogger(DadosSingletonServidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void removerCliente(InterfaceCliente cliente) {
        for (InterfaceCliente intf : this.clientes) {
            clientes.remove(intf);
            System.out.println("Cliente removido, agora a lista tem tamanho " + clientes.size());
        }
    }

    public void addInteresse(InteresseBean inter) {
        interesses.add(inter);
        addInteresseNaTabelaEdicao(inter);
    }

    public void avisarInteressados(Constantes.TipoInteresse tipo, String msg) throws RemoteException {
        for (InteresseBean inte : interesses) {
            if (inte.getInteresse() == tipo && new Date().before(inte.getExpiracao())) {
                try {
                    //avisar
                    inte.getCliente().avisarInteresse(tipo, msg);
                } catch (RemoteException ex) {
                    Logger.getLogger(DadosSingletonServidor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void broadcastNovasHospedagens() throws RemoteException {
        for (InterfaceCliente intf : this.clientes) {
            intf.atualizarHospedagens(this.diarias);
        }
    }

    public void broadcastNovasPassagens() {
        System.out.println("Não implementado, outros métodos já fazem isso.");
    }

    public void addDiaria(HospedagemBean novaDiaria) {
        this.diarias.add(novaDiaria);
        try {
            avisarInteressados(Constantes.TipoInteresse.HOSPEDAGEM_NOVA, "Uma nova hospedagem está disponível!");
        } catch (RemoteException ex) {
            Logger.getLogger(DadosSingletonServidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void atualizarTabelaPassagem() {
        //Remove todas as linhas da tabela de passagem
        int size = tabelaDePassagens.getRowCount();
        for (int i = 0; i < size; i++) {
            this.tabelaDePassagens.removeRow(0);
        }
        //Coloca todas as linhas novamente, atualizadas, com base na lista de passagens
        for (PassagemBean pass : passagens) {
            addPassagemNaTabelaEdicao(pass);
        }
    }

    void editarHospedagem(HospedagemBean hospedagem) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static class SingletonListaDeClientesHolder {

        private static final DadosSingletonServidor INSTANCE = new DadosSingletonServidor();
    }
    //Fim dos métodos

    public List<InterfaceCliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<InterfaceCliente> clientes) {
        this.clientes = clientes;
    }

    public List<PassagemBean> getPassagensDisponiveis() {
        List<PassagemBean> passagensDisponiveis = new ArrayList();
        for (PassagemBean pass : this.passagens) {
            if (pass.getPassageiroString().equals("")) {
                passagensDisponiveis.add(pass);
            }
        }
        return passagensDisponiveis;
    }

    public PassagemBean getPassagem(int indice) {
        return this.passagens.get(indice);
    }

    public HospedagemBean getDiaria(int indice) {
        return this.diarias.get(indice);
    }

    public int getPassagensSize() {
        return passagens.size();
    }

    public void addPassagem(PassagemBean nova) {
        this.passagens.add(nova);
        addPassagemNaTabelaEdicao(nova);
        try {
            avisarInteressados(Constantes.TipoInteresse.PASSAGEM_NOVA, "Uma nova passagem está disponível!");
        } catch (RemoteException ex) {
            Logger.getLogger(DadosSingletonServidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addPassagemNaTabelaEdicao(PassagemBean nova) {
        //origem destino preço data pagador passageiro parcelas
        Object[] o = new Object[6];
        o[1] = nova.getDestino();
        o[2] = nova.getDataString();
        o[3] = nova.getPreco();
        o[4] = nova.getPassageiroString();
        o[5] = nova.getNumeroDeParcelas();
        this.tabelaDePassagens.addRow(o);
    }

    public void addInteresseNaTabelaEdicao(InteresseBean nova) {
        //origem destino preço data pagador passageiro parcelas
        Object[] o = new Object[5];
        o[0] = nova.getInteresse();
        o[1] = nova.getExpiracao();
        this.tabelaInteresses.addRow(o);
    }

    public DefaultTableModel getTabelaInteresses() {
        return tabelaInteresses;
    }

    public void setTabelaInteresses(DefaultTableModel tabelaInteresses) {
        this.tabelaInteresses = tabelaInteresses;
    }

    public List<HospedagemBean> getDiarias() {
        return diarias;
    }

    public void setDiarias(List<HospedagemBean> diarias) {
        this.diarias = diarias;
    }

    public List<InteresseBean> getInteresses() {
        return interesses;
    }

    public void setInteresses(List<InteresseBean> interesses) {
        this.interesses = interesses;
    }

    public DefaultTableModel getTabelaDePassagens() {
        return tabelaDePassagens;
    }

    public void setTabelaDePassagens(DefaultTableModel tabelaDePassagens) {
        this.tabelaDePassagens = tabelaDePassagens;
    }

}
