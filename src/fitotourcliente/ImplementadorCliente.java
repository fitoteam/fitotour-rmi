/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitotourcliente;

import constantes.Constantes;
import constantes.HospedagemBean;
import constantes.TabelaHospedagem;
import interfaces.InterfaceCliente;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Tito
 */
public class ImplementadorCliente extends UnicastRemoteObject implements InterfaceCliente {

    public ImplementadorCliente() throws RemoteException {

    }

    @Override
    public void echo(String qualquer) throws RemoteException {
        System.out.println("String recebida: " + qualquer);
    }

    @Override
    public void atualizarTabelaDePassagens() throws RemoteException {
        DadosSingletonCliente.getInstance().atualizarTabelaDePassagens();
    }

    @Override
    public void atualizarTabelaDeHospedagens() throws RemoteException {
        DadosSingletonCliente.getInstance().atualizarTabelaDeHospedagens();
    }

    @Override
    public void avisarPassagemNova(String info) throws RemoteException {
        JOptionPane.showMessageDialog(DadosSingletonCliente.getInstance().janelaPrincipal.getComponent(0), info, "Passagem nova disponível.", JOptionPane.INFORMATION_MESSAGE);

    }

    @Override
    public void avisarPassagemBarata(String info) throws RemoteException {
        JOptionPane.showMessageDialog(DadosSingletonCliente.getInstance().janelaPrincipal.getComponent(0), info, "Passagem mais barata disponível!", JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    public void avisarInteresse(Constantes.TipoInteresse tipo, String msg) throws RemoteException {
        switch (tipo) {
            case HOSPEDAGEM_BARATA:
                JOptionPane.showMessageDialog(DadosSingletonCliente.getInstance().janelaPrincipal.getComponent(0), msg, "Diaria mais barata disponível!", JOptionPane.INFORMATION_MESSAGE);
                break;
            case HOSPEDAGEM_NOVA:
                JOptionPane.showMessageDialog(DadosSingletonCliente.getInstance().janelaPrincipal.getComponent(0), msg, "Diaria nova disponível.", JOptionPane.INFORMATION_MESSAGE);
                break;
            case PASSAGEM_BARATA:
                JOptionPane.showMessageDialog(DadosSingletonCliente.getInstance().janelaPrincipal.getComponent(0), msg, "Passagem mais barata disponível!", JOptionPane.INFORMATION_MESSAGE);
                break;
            case PASSAGEM_NOVA:
                JOptionPane.showMessageDialog(DadosSingletonCliente.getInstance().janelaPrincipal.getComponent(0), msg, "Passagem nova disponível.", JOptionPane.INFORMATION_MESSAGE);
                break;
        }
    }

    @Override
    public void atualizarHospedagens(List<HospedagemBean> diarias) throws RemoteException {
        System.out.println("Atualizar hospedagens chamado no cliente. Lista de hospedagens sendo atualizadas e tabela sendo atualizada.");
        DadosSingletonCliente.getInstance().diarias = diarias;
        DadosSingletonCliente.getInstance().tabelaHospedagem.setDiarias(diarias);
        DadosSingletonCliente.getInstance().tabelaHospedagem.fireTableDataChanged();
        System.out.println("Num linhas nova tabela diarias: "+ diarias.size()+" e colunas: " + DadosSingletonCliente.getInstance().tabelaHospedagem.getColumnCount());
    }
}