/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitotourcliente;

import interfaces.InterfaceServidor;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 *
 * @author Tito
 */
public class FitoTourCliente {

    /**
     * @param args the command line arguments
     * @throws java.rmi.RemoteException
     * @throws java.rmi.NotBoundException
     */
    public static void main(String[] args) throws RemoteException, NotBoundException {
            Registry referenciaServicoNomes = LocateRegistry.getRegistry();
            InterfaceServidor refServidor = (InterfaceServidor) referenciaServicoNomes.lookup("SERVIDOR");
            refServidor.batata();
            refServidor.chamar("Cliente", new ImplementadorCliente());
    }

}
