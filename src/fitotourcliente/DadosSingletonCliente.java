/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitotourcliente;

import constantes.ClienteUsuario;
import constantes.HospedagemBean;
import constantes.InteresseBean;
import constantes.PassagemBean;
import constantes.TabelaHospedagem;
import interfaces.InterfaceServidor;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author feliperdelara
 */
public final class DadosSingletonCliente {
    
    public InterfaceServidor servidor;
    public DefaultTableModel tabelaPassagemCliente;
    public ImplementadorCliente implementadorCliente;
    public javax.swing.JFrame janelaPrincipal;
    public ClienteUsuario clienteUsuario;
    public List<HospedagemBean> diarias = new ArrayList();
    public TabelaHospedagem tabelaHospedagem;

    //Métodos necessários para o singleton
    private DadosSingletonCliente() {
        iniciarReferenciaServidor();
        try {
            implementadorCliente = new ImplementadorCliente();
            servidor.oiCliente(implementadorCliente);
        } catch (RemoteException ex) {
            Logger.getLogger(DadosSingletonCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void iniciarReferenciaServidor() {
        try {
            Registry referenciaServicoNomes;
            
            referenciaServicoNomes = LocateRegistry.getRegistry();
            this.servidor = (InterfaceServidor) referenciaServicoNomes.lookup("SERVIDOR");
            
            System.out.println("Conectado ao servidor.");
        } catch (RemoteException | NotBoundException ex) {
            Logger.getLogger(DadosSingletonCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<HospedagemBean> getDiarias() {
        return diarias;
    }
    
    public void setDiarias(List<HospedagemBean> diarias) {
        this.diarias = diarias;
    }
    
    public void atualizarTabelaDePassagens() {
        try {
            List<PassagemBean> passagens;
            passagens = DadosSingletonCliente.getInstance().servidor.listarPassagensDisponiveis();
            
            System.out.println("Tabela passagens sendo atualizadas. Tamanho lista recebida:" + passagens.size());
            
            DefaultTableModel model = tabelaPassagemCliente;
            System.out.println("Numero de linhas na tabela: " + model.getRowCount());
            System.out.println("Removendo linhas:");
            int tam = model.getRowCount();
            for (int i = 0; i < tam; i++) {
                System.out.println("Iteração n.o " + i + " de " + tam);
                model.removeRow(0);
                System.out.println("Linha removida, Numero atual de linhas na tabela: " + model.getRowCount());
            }
            for (PassagemBean adv : passagens) {
                Object[] o = new Object[4];
                o[0] = adv.getOrigem();
                o[1] = adv.getDestino();
                o[2] = converterDataPassagem(adv.getData());
                o[3] = "" + adv.getPreco();
                model.addRow(o);
            }
        } catch (RemoteException ex) {
            Logger.getLogger(DadosSingletonCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String converterDataPassagem(java.util.Date data) {
        return new java.text.SimpleDateFormat("dd/MM/yyyy").format(data);
    }
    
    public java.util.Date converterDataExpiracao(String data) {
        try {
            return new SimpleDateFormat("dd/MM/yyyy").parse(data);
        } catch (ParseException ex) {
            Logger.getLogger(DadosSingletonCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void enviarInteresse(constantes.Constantes.TipoInteresse tipo, String date) {
        InteresseBean inter = new InteresseBean(implementadorCliente, tipo, converterDataExpiracao(date));
        try {
            servidor.adicionarInteresse(inter);
        } catch (RemoteException ex) {
            Logger.getLogger(DadosSingletonCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static DadosSingletonCliente getInstance() {
        return SingletonListaDeClientesHolder.INSTANCE;
    }
    
    void atualizarTabelaDeHospedagens() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

//    public HospedagemBean getDiaria(int indice) {
//        return this.diarias.get(indice);
//    }
    public PassagemBean getPassagem(int selectedRow) {
        try {
            List<PassagemBean> passagens;
            passagens = DadosSingletonCliente.getInstance().servidor.listarPassagensDisponiveis();
            
            return passagens.get(selectedRow);
        } catch (RemoteException ex) {
            Logger.getLogger(DadosSingletonCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void enviarCompraPassagem(PassagemBean passagem, int index) {
        try {
            servidor.editarPassagem(passagem, index);
            System.out.println("Passagem editada agora pertence a " + passagem.getPassageiroString());
        } catch (RemoteException ex) {
            Logger.getLogger(DadosSingletonCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void enviarCompraHospedagem(HospedagemBean escolhida) {
        try {
            servidor.editarHospedagem(escolhida);
        } catch (RemoteException ex) {
            Logger.getLogger(DadosSingletonCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static class SingletonListaDeClientesHolder {
        
        private static final DadosSingletonCliente INSTANCE = new DadosSingletonCliente();
    }
    //Fim dos métodos

}
