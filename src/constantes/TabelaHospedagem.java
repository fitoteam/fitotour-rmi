/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantes;

import fitotourservidor.DadosSingletonServidor;
import fitotourservidor.janelas.JanelaPrincipalServidor;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Tito
 */
public class TabelaHospedagem extends AbstractTableModel {

    private List<HospedagemBean> diarias = new ArrayList();
    private boolean tabelaCompleta;
    private String[] colunas;

    public static enum NomeColunas {
        CIDADE(0, "Cidade"), DIA(1, "Dia"), CAPACIDADE(2, "Capacidade"),
        PRECO(3, "Preço"), OCUPANTE(4, "Ocupante"), N_PARCELAS(5, "N.o Parcelas"),
        N_CARTAO(6, "N.o Cartão");
        public int indice;
        public String nome;

        NomeColunas(int val, String nom) {
            this.indice = val;
            this.nome = nom;
        }

        public String getNome() {
            return nome;
        }

        public int getIndice() {
            return indice;
        }
    }

    public TabelaHospedagem(List<HospedagemBean> diarias, boolean tabelaCompleta) {
        this.tabelaCompleta = tabelaCompleta;
        if (tabelaCompleta) {
            this.diarias = diarias;
            //Inicia a tabelea com todas as colunas
            colunas = new String[]{NomeColunas.CIDADE.nome,
                NomeColunas.DIA.nome, NomeColunas.CAPACIDADE.nome,
                NomeColunas.PRECO.nome, NomeColunas.OCUPANTE.nome,
                NomeColunas.N_PARCELAS.nome, NomeColunas.N_CARTAO.nome};
        } else {
            for(HospedagemBean hosp : diarias){
                if(hosp.getOcupante()!=null)
                    diarias.add(hosp);
            }
            //Inicia a tabela com apenas as colunas necessárias
            colunas = new String[]{NomeColunas.CIDADE.nome,
                NomeColunas.DIA.nome, NomeColunas.CAPACIDADE.nome,
                NomeColunas.PRECO.nome};
        }
    }

    @Override
    public int getRowCount() {
        if (this.tabelaCompleta) {
            return diarias.size();
        } else {
            int cont = 0;
            for (HospedagemBean diaria : diarias) {
                if (diaria.getOcupante() == null) {
                    cont++;
                }
            }
            return cont;
        }
    }

    @Override
    public int getColumnCount() {
        if (this.tabelaCompleta) {
            return colunas.length;
        } else if (colunas.length != 0) {
            return colunas.length;
        } else {
            System.out.println("Erro no calculo das colunas!!!!!!!!");
            return 4;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (tabelaCompleta == false && columnIndex > colunas.length) {
            System.out.println("Índice inválido");
        }
        HospedagemBean diaria = diarias.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return diaria.getCidade();
            case 1:
                return Constantes.dataParaString(diaria.getDia());
            case 2:
                return diaria.getCapacidade();
            case 3:
                return diaria.getPreco();
            case 4:
                if (diaria.getOcupante() == null) {
                    return "";
                } else {
                    return diaria.getOcupante().getNome();
                }
            case 5:
                return diaria.getNumeroDeParcelas();
            case 6:
                return diaria.getCartao();
        }
        System.out.println("Índice inválido");
        return null;
    }

    @Override
    public void setValueAt(Object alteracao, int linha, int coluna) {
        HospedagemBean diaria = diarias.get(linha);
        int precoNovo = Integer.parseInt(alteracao.toString());
        int precoAntigo = diaria.getPreco();
        try {
            if (coluna == NomeColunas.PRECO.indice) {
                if (precoNovo < precoAntigo && diaria.getOcupante() == null) {
                    //Preço abaixou!!!
                    System.out.println("O preço abaixou");
                    diaria.setPreco(precoNovo);
                    fireTableCellUpdated(linha, coluna);
                    String texto = "A diaria da cidade " + diaria.getCidade() + " baixou de preço, confira!";
                    try {
                        DadosSingletonServidor.getInstance().avisarInteressados(Constantes.TipoInteresse.HOSPEDAGEM_BARATA, texto);
                        DadosSingletonServidor.getInstance().broadcastNovasHospedagens();
                    } catch (RemoteException ex) {
                        Logger.getLogger(JanelaPrincipalServidor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                if (precoNovo == precoAntigo) {
                    //Preço igual
                    System.out.println("O preço nao mudou");
                }
                if (precoNovo > precoAntigo) {
                    //Preço aumentou
                    System.out.println("O preço aumentou");
                    diaria.setPreco(precoNovo);
                    fireTableCellUpdated(linha, coluna);
                    try {
                        DadosSingletonServidor.getInstance().broadcastNovasHospedagens();
                    } catch (RemoteException ex) {
                        Logger.getLogger(TabelaHospedagem.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                DadosSingletonServidor.getInstance().atualizarTabelaPassagemClientes();
            }
        } catch (NumberFormatException e) {
            System.out.println("" + e);
        }
    }

    @Override
    public String getColumnName(int columnIndex) {
        return colunas[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    public void mudarPreco(int preco, int linha) {
        diarias.get(linha).setPreco(preco);
        fireTableRowsUpdated(linha, linha);
    }

    public HospedagemBean getDiaria(int linha) {
        return diarias.get(linha);
    }

    public void limpar() {
        diarias.clear();
        fireTableDataChanged();
    }

    public void addRow(HospedagemBean nova) {
        diarias.add(nova);
        fireTableRowsInserted(diarias.size(), diarias.size());
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        //Editavel se for coluna preço, e for tabela de edição e não ter sido vendida ainda
        return col == NomeColunas.PRECO.indice && tabelaCompleta == true && getDiaria(row).getOcupante()==null;
    }

    public void setDiarias(List<HospedagemBean> diar) {
        diarias = diar;
        fireTableDataChanged();
    }
}
