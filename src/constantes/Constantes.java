/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package constantes;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tito
 */
public final class Constantes {
    public static final String REFERENCIA_SERVIDOR = "SERVIDOR";
            
    public enum TipoInteresse {
        PASSAGEM_NOVA, PASSAGEM_BARATA, HOSPEDAGEM_NOVA, HOSPEDAGEM_BARATA
    }
    
    public static Date stringParaData(String stringData){
        
        try {
            // String string = "January 2, 2010";
            // DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            // LocalDate date = LocalDate.parse(dataString, formatter);
            // System.out.println(date); // 2010-01-02
            
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            Date date = format.parse(stringData);
            
   
            return date;
        } catch (ParseException ex) {
            Logger.getLogger(Constantes.class.getName()).log(Level.SEVERE, null, ex);
        }
            return null;
    }
    
    public static String dataParaString(Date data){
        return new SimpleDateFormat("dd/MM/yyyy").format(data);
    }
}
