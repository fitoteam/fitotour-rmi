/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantes;

import fitotourcliente.DadosSingletonCliente;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Tito
 */
public class HospedagemBean implements Serializable {

    private String cidade;
    private java.util.Date dia;
    private int preco;
    private int capacidade;
    private ClienteBean ocupante;
    private int numeroDeParcelas;
    private String cartao;

    public HospedagemBean() {
    }

    public HospedagemBean(String cidade, Date dia, int preco, int capacidade, ClienteBean ocupante, int numeroDeParcelas) {
        this.cidade = cidade;
        this.dia = dia;
        this.preco = preco;
        this.capacidade = capacidade;
        this.ocupante = ocupante;
        this.numeroDeParcelas = numeroDeParcelas;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public Date getDia() {
        return dia;
    }
    
    public String getDiaString() {
        return DadosSingletonCliente.getInstance().converterDataPassagem(dia);
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public int getPreco() {
        return preco;
    }

    public void setPreco(int preco) {
        this.preco = preco;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    public ClienteBean getOcupante() {
        return ocupante;
    }

    public void setOcupante(ClienteBean ocupante) {
        this.ocupante = ocupante;
    }

    public String getCartao() {
        return cartao;
    }

    public void setCartao(String cartao) {
        this.cartao = cartao;
    }

    public int getNumeroDeParcelas() {
        return numeroDeParcelas;
    }

    public void setNumeroDeParcelas(int numeroDeParcelas) {
        this.numeroDeParcelas = numeroDeParcelas;
    }
}
