/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantes;

import fitotourcliente.DadosSingletonCliente;
import interfaces.InterfaceCliente;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Tito
 */
public class InteresseBean implements Serializable{
    private InterfaceCliente cliente = DadosSingletonCliente.getInstance().implementadorCliente;
    private Constantes.TipoInteresse interesse;
    private java.util.Date expiracao;

    public InteresseBean(InterfaceCliente cliente, Constantes.TipoInteresse interesse, Date expiracao) {
        this.cliente = cliente;
        this.interesse = interesse;
        this.expiracao = expiracao;
    }

    public InterfaceCliente getCliente() {
        return cliente;
    }

    public void setCliente(InterfaceCliente cliente) {
        this.cliente = cliente;
    }

    public Constantes.TipoInteresse getInteresse() {
        return interesse;
    }

    public void setInteresse(Constantes.TipoInteresse interesse) {
        this.interesse = interesse;
    }

    public Date getExpiracao() {
        return expiracao;
    }

    public void setExpiracao(Date expiracao) {
        this.expiracao = expiracao;
    }
    
    
    
}

