/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantes;

import java.io.Serializable;

/**
 *
 * @author Tito
 */
public class ClienteBean implements Serializable {

    private String nome;
    private int idade;

    public ClienteBean() {
    }

    public ClienteBean(String nome, int idade) {
        this.nome = nome;
        this.idade = idade;
    }

    public String getNome() {
        if (nome == null) {
            return "";
        } else {
            return nome;
        }
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }
    
    

}
