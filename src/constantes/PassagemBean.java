/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantes;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Tito
 */
public class PassagemBean implements Serializable {

    private String origem;
    private String destino;
    private java.util.Date data;
    private int preco;
    private ClienteBean passageiro;
    private int numeroDeParcelas;
    private String cartao;

    public PassagemBean() {
    }

    public PassagemBean(String origem, String destino, Date data, int preco, ClienteBean passageiro, int numeroDeParcelas) {
        this.origem = origem;
        this.destino = destino;
        this.data = data;
        this.preco = preco;
        this.passageiro = passageiro;
        this.numeroDeParcelas = numeroDeParcelas;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Date getData() {
        return data;
    }

    public String getDataString() {
        return new SimpleDateFormat("dd/MM/yyyy").format(data);
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getPreco() {
        return preco;
    }

    public void setPreco(int preco) {
        this.preco = preco;
    }

    public String getPassageiroString() {
        if (passageiro == null) {
            return "";
        } else {
            return passageiro.getNome();
        }
    }

    public void setPassageiro(ClienteBean passageiro) {
        this.passageiro = passageiro;
    }
    
    public ClienteBean getPassageiro(){
        return this.passageiro;
    }

    public int getNumeroDeParcelas() {
        return numeroDeParcelas;
    }

    public void setNumeroDeParcelas(int numeroDeParcelas) {
        this.numeroDeParcelas = numeroDeParcelas;
    }

    public String getCartao() {
        return cartao;
    }

    public void setCartao(String cartao) {
        this.cartao = cartao;
    }
    
    

}
