/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantes;

import interfaces.InterfaceCliente;
import java.io.Serializable;

/**
 *
 * @author Tito
 */
public class ClienteUsuario extends ClienteBean implements Serializable{
    private InterfaceCliente referencia;

    public ClienteUsuario(String nome, int idade) {
        super(nome, idade);
    }
    
    public ClienteUsuario(InterfaceCliente referencia, String nome, int idade) {
        super(nome, idade);
        this.referencia = referencia;
    }

    public InterfaceCliente getReferencia() {
        return referencia;
    }

    public void setReferencia(InterfaceCliente referencia) {
        this.referencia = referencia;
    }
    
}
