/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import constantes.HospedagemBean;
import constantes.InteresseBean;
import constantes.PassagemBean;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 *
 * @author Tito
 */
public interface InterfaceServidor extends Remote {
    void chamar(String nomeCliente, InterfaceCliente intfCliente) throws RemoteException;
    void batata() throws RemoteException;
    void oiCliente(InterfaceCliente cliente) throws RemoteException;
    void removerCliente(InterfaceCliente cliente) throws RemoteException;
    List<PassagemBean> listarPassagensDisponiveis() throws RemoteException;
    void adicionarInteresse(InteresseBean inter) throws RemoteException;
    void editarPassagem(PassagemBean passagem, int index) throws RemoteException;
    void editarHospedagem(HospedagemBean hospedagem) throws RemoteException;
    List<HospedagemBean> getHospedagens() throws RemoteException;
    void chamarBroadcastHospedagens() throws RemoteException;
}
