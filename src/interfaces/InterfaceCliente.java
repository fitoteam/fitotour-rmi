/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import constantes.Constantes;
import constantes.HospedagemBean;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 *
 * @author Tito
 */
public interface InterfaceCliente extends Remote {

    void echo(String qualquer) throws RemoteException;
    void atualizarTabelaDePassagens() throws RemoteException;
    void atualizarTabelaDeHospedagens() throws RemoteException;
    void avisarPassagemNova(String info) throws RemoteException;
    void avisarPassagemBarata(String info) throws RemoteException;
    void avisarInteresse(Constantes.TipoInteresse tipo, String msg) throws RemoteException;
    void atualizarHospedagens(List<HospedagemBean> diarias) throws RemoteException;
}
